Feature: Add products to shopping car in ecommerce

  Scenario: Add products to shopping car successfully
    Given a customer sing in into the ecommerce site
    When the customer adds products to shopping car
    Then the customer should see the checkout successfully

  Scenario: Go to checkout without products in the shopping car
    Given a customer sing in into the ecommerce site
    When the customer goes to checkout with the shopping car empty
    Then the customer should see label empty car