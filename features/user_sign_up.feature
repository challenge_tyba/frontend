Feature: Users creation in ecommerce site

  Scenario: Successful user creation
    Given a customer in the ecommerce home site
    When the customer complete all information to create the user
    Then the customer should be create a new user successfully

  Scenario: Failed user creation when term and conditions are not accepted
    Given a customer in the ecommerce home site
    When the customer complete all information to create the user without accepted term and conditions
    Then the customer should see registration button disabled

  Scenario: Failed user creation when the form is incomplete
    Given a customer in the ecommerce home site
    When the customer has any input empty in the form to create the user
    Then the customer should see registration button disabled