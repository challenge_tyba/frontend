Feature: Payment purchase process in the ecommerce

  Scenario: Payment purchase successfully
    Given a customer sing in into the ecommerce site
    And the customer adds products to shopping car
    When the customer completes the payment for the purchase
    Then the customer should be successfully payment process