# Challenge automation fronted Tyba 2022

Este proyecto esta construido en Python bajo el  patrón de automatización 
screenplay lo cual es posible gracias a la libreria ScreenPy.
En el proyecto se implementaron algunos casos de prueba para
probar funcionalidades del ecommerce disponible en
https://www.advantageonlineshopping.com/#/

Para ejecutar el proyecto se sugiere crear un nuevo ambiente virtual de python y 
en el instalar las librerías que se encuentran en el archivo requirements.txt.

Estando en el directorio raíz del proyecto y con python 3 instalado se pueden ejecutar 
los siguientes comando en sistemas unix: 

```
1. pyhton3 -v venv env -> Crear ambiente virtual

2. source env/bin/activate -> Activar ambiente virtual

3. pip install -r requirements.txt -> Instalar modulos requeridos

4. python3 -m pytest -v tests/ --alluredir allure_report -> Ejecutar los tests
```

La ejecución de las pruebas genera un reporte de **allure**  que para visualizarlo 
se requiere tener este componente en la máquina.

https://github.com/allure-framework/allure2
https://qameta.io/allure-report/

El reporte se genera mediante el siguiente comando una vez instalada la herramienta
de allure:

```
allure serve allure_report
```

Las librerías usadas en este proyecto son 

- ScreenPy -> https://pypi.org/project/screenpy/
  (Permite la implemetación de screenplay)
- Faker -> https://pypi.org/project/Faker/
  (Generación de datos aletorios)
- Web Driver Manager -> https://pypi.org/project/webdrivermanager/
  (Gestión y autodescarga del driver)
- Pytest BDD -> https://pypi.org/project/pytest-bdd/
  (Gestión y uso de los features)

## Los casos de prueba implementados son:

### Users creation in ecommerce site:
- Successful user creation
- Failed user creation when term and conditions are not accepted
- Failed user creation when the form is incomplete

### Add products to shopping car in ecommerce:
- Add products to shopping car successfully
- Go to checkout without products in the shopping car

### Payment purchase process in the ecommerce:
- Payment purchase successfully
  -> ToDo: Seleccionar medio de pago y completar el pago posterior a la solución 
  de bug existente