import random

from screenpy import AnActor
from screenpy.abilities import BrowseTheWeb
from screenpy.actions import Click, Wait, Pause
from screenpy.pacing import beat
from selenium.webdriver.common.by import By

import user_interface.home_page as home_page
import user_interface.product_page as product_page
from tasks.commons.take_screenshot import Take
from tasks.ecommerce.select_popular_items import SelectPopular
from utils.constants.waits_constants import ONE, TWO, FIVE, TEN


class AddPopularProducts:

    @staticmethod
    def to_shopping_car() -> "AddPopularProducts":
        return AddPopularProducts()

    @beat("{} adds any popular products to shopping car")
    def perform_as(self, the_actor: AnActor) -> None:
        the_actor.attempts_to(
            SelectPopular.items()
        )
        driver = the_actor.ability_to(BrowseTheWeb).browser
        popular_items = driver.find_elements(
            By.XPATH, product_page.XPATH_DETAILS_POPULAR_ITEMS
        )
        total_product_to_add: int = random.randint(1, len(popular_items))
        for i in range(total_product_to_add):
            popular_items = driver.find_elements(
                By.XPATH, product_page.XPATH_DETAILS_POPULAR_ITEMS
            )
            popular_items[i-1].click()
            the_actor.attempts_to(
                Pause.for_(TWO).second_because("product details take a seconds to appear"),
                Take.screenshot()
            )
            total_unit = random.randint(1, 20)
            for _ in range(total_unit):
                the_actor.attempts_to(
                    Click.on_the(product_page.BTN_PLUS_PRODUCT),
                    Take.screenshot()
                )
            the_actor.attempts_to(
                Click.on_the(product_page.BTN_SAVE_TO_CAR),
                Wait(FIVE).seconds_for_the(product_page.BTN_CHECK_OUT),
                Take.screenshot(),
                Pause.for_(TWO).second_because("check out model take a seconds to appear"),
                Click.on_the(product_page.BTN_CHECK_OUT),
                Wait(TEN).seconds_for_the(product_page.USER_CAR),
                Pause.for_(ONE).second_because("home options take milliseconds to appear"),
                Click.on_the(product_page.BTN_GO_TO_HOME),
                Take.screenshot(),
                Wait(FIVE).seconds_for_the(home_page.OPTION_POPULAR_ITEMS),
                SelectPopular.items()
            )
