from faker import Faker
from screenpy import AnActor
from screenpy.actions import Click, Wait, Pause, Enter, MakeNote
from screenpy.pacing import beat

import user_interface.sing_up_page as sing_up_page
from tasks.commons.take_screenshot import Take
from utils.constants.waits_constants import TWO, FIVE


class FillOut:

    @staticmethod
    def sing_up_form() -> "FillOut":
        return FillOut()

    @beat("{} fills out sign up form")
    def perform_as(self, the_actor: AnActor) -> None:
        fake_data: Faker = Faker(["es-CO"])
        username = fake_data.user_name()[0:14]
        password: str = fake_data.password()
        city: str = fake_data.city()
        short_city: str = city[0:8]

        the_actor.attempts_to(
            MakeNote.of(username).as_("username"),
            Wait(FIVE).seconds_for_the(sing_up_page.BTN_USER),
            Click.on_the(sing_up_page.BTN_USER),
            Pause.for_(TWO).second_because("modal create account takes seconds to apper"),
            Wait(FIVE).seconds_for_the(sing_up_page.BTN_CREATE_NEW_ACCOUNT),
            Click.on_the(sing_up_page.BTN_CREATE_NEW_ACCOUNT),
            Take.screenshot(),
            Wait(FIVE).seconds_for_the(sing_up_page.FORM_CREATE_NEW_ACCOUNT),
            Enter.the_text(username).into_the(sing_up_page.INPUT_USERNAME),
            Enter.the_text(fake_data.email()).into_the(sing_up_page.INPUT_EMAIL),
            Enter.the_text(password).into_the(sing_up_page.INPUT_PASSWORD),
            Enter.the_text(password).into_the(sing_up_page.INPUT_CONFIRM_PASSWORD),
            Take.screenshot(),
            Enter.the_text(fake_data.first_name()).into_the(sing_up_page.INPUT_FIRST_NAME),
            Enter.the_text(fake_data.last_name()).into_the(sing_up_page.INPUT_LAST_NAME),
            Enter.the_text(fake_data.phone_number()).into_the(sing_up_page.INPUT_PHONE_NUMBER),
            Enter.the_text(city).into_the(sing_up_page.INPUT_CITY),
            Enter.the_text(fake_data.street_address()).into_the(sing_up_page.INPUT_ADDRESS),
            Enter.the_text(short_city).into_the(sing_up_page.INPUT_STATE),
            Enter.the_text(fake_data.postcode()).into_the(sing_up_page.INPUT_POSTAL_CODE),
            Take.screenshot()
        )
