from screenpy import AnActor
from screenpy.actions import Open
from screenpy.pacing import beat

from tasks.commons.take_screenshot import Take
from utils.configurations.read_test_configuration import ReadParameters


class GotoEcommerce:

    @staticmethod
    def home() -> "GotoEcommerce":
        return GotoEcommerce()

    @beat("{} goes to the ecommerce home url")
    def perform_as(self, the_actor: AnActor) -> None:
        ecommerce_home_url: str = ReadParameters.read_parameter("WEB_PROPERTIES", "ecommerce_home_url")
        the_actor.was_able_to(
            Open.their_browser_on(ecommerce_home_url),
            Take.screenshot()
        )
