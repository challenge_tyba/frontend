from screenpy import AnActor
from screenpy.actions import Click, Pause
from screenpy.pacing import beat

import user_interface.home_page as home_page
from tasks.commons.take_screenshot import Take
from utils.constants.waits_constants import TWO


class SelectPopular:

    @staticmethod
    def items() -> "SelectPopular":
        return SelectPopular()

    @beat("{} selects popular items")
    def perform_as(self, the_actor: AnActor) -> None:
        the_actor.attempts_to(
            Click.on_the(home_page.OPTION_POPULAR_ITEMS),
            Take.screenshot(),
            Pause.for_(TWO).second_because("popular items take a milliseconds to appear")
        )
