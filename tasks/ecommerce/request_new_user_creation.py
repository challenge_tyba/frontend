from screenpy import AnActor
from screenpy.actions import Click, See
from screenpy.pacing import beat
from screenpy.questions.attribute import Attribute
from screenpy.resolutions.is_equal_to import IsEqualTo

import user_interface.sing_up_page as sing_up_page
from tasks.commons.take_screenshot import Take


class RequestUser:

    @staticmethod
    def creation() -> "RequestUser":
        return RequestUser()

    @beat("{} requests new user creation")
    def perform_as(self, the_actor: AnActor) -> None:

        the_actor.attempts_to(
            Click.on_the(sing_up_page.CHECK_BOX_AGREE),
            Take.screenshot()
        )

        the_actor.should(
            See.the(
                Attribute("disabled").of_the(sing_up_page.BTN_REGISTER), IsEqualTo(None))
        )

        the_actor.attempts_to(
            Click.on_the(sing_up_page.BTN_REGISTER),
            Take.screenshot()
        )
