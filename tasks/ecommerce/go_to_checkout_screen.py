from screenpy import AnActor
from screenpy.actions import Click, Wait, Pause
from screenpy.pacing import beat

import user_interface.product_page as product_page
from tasks.commons.take_screenshot import Take
from utils.constants.waits_constants import TWO, FIVE, TEN


class GoToCheckOut:

    @staticmethod
    def screen() -> "GoToCheckOut":
        return GoToCheckOut()

    @beat("{} goes to checkout screen")
    def perform_as(self, the_actor: AnActor) -> None:
        the_actor.attempts_to(
            Wait(FIVE).seconds_for_the(product_page.MENU_CAR),
            Click.on_the(product_page.MENU_CAR),
            Wait(FIVE).seconds_for_the(product_page.BTN_CHECK_OUT),
            Take.screenshot(),
            Pause.for_(TWO).second_because("check out model take a seconds to appear"),
            Click.on_the(product_page.BTN_CHECK_OUT),
            Wait(TEN).seconds_for_the(product_page.USER_CAR),
        )
