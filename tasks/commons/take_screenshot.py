from allure_commons.types import AttachmentType
from screenpy import AnActor
from screenpy.actions import SaveScreenshot
from screenpy.pacing import beat

from utils.constants.general_constants import FILE_NAME_TEMP_SCREENSHOT


class Take:

    @staticmethod
    def screenshot() -> "Take":
        return Take()

    @beat("{} takes a screenshot as evidence")
    def perform_as(self, the_actor: AnActor) -> None:
        the_actor.attempts_to(
            SaveScreenshot.as_(FILE_NAME_TEMP_SCREENSHOT).and_attach_it_with(
                attachment_type=AttachmentType.PNG,
            )
        )