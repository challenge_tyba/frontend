from screenpy import AnActor
from screenpy.abilities.browse_the_web import BrowseTheWeb
from screenpy.pacing import beat

from tasks.commons.take_screenshot import Take


class End:

    @staticmethod
    def test() -> "End":
        return End()

    @beat("the task is finished")
    def perform_as(self, the_actor: AnActor) -> None:
        the_actor.attempts_to(Take.screenshot())
        the_actor.uses_ability_to(BrowseTheWeb).forget()
