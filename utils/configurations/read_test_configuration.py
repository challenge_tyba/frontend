import configparser

from exceptions.test_configuration_exception import TestConfigurationException


class ReadParameters:

    @staticmethod
    def read_parameter(name_block_parameters, name_parameter) -> str:
        try:
            config = configparser.ConfigParser()
            config.read("./utils/configurations/test_configuration.ini")
            return str(config.get(name_block_parameters, name_parameter))
        except TestConfigurationException:
            raise TestConfigurationException(
                f"""An error occurred while trying to read the file test_configuration.ini, 
               please check verify that the file exists or that contains the parameter {name_parameter}"""
            )
