import pytest
from pytest_bdd import scenarios, given, when, then
from screenpy import AnActor
from screenpy.abilities import BrowseTheWeb
from screenpy.actions import See
from driver.driver_manager import DriverManager
from tasks.commons.end_test import End

from tasks.ecommerce.go_to_ecommerce_site import GotoEcommerce
from tasks.ecommerce.fill_up_sign_up_form import FillOut
from tasks.ecommerce.request_new_user_creation import RequestUser
from tasks.ecommerce.add_products_to_car import AddPopularProducts
from tasks.ecommerce.go_to_checkout_screen import GoToCheckOut
from tasks.commons.take_screenshot import Take
from screenpy.actions import Wait
import user_interface.home_page as home_page
import user_interface.checkout_page as checkout_page
from utils.constants.waits_constants import FIVE
from screenpy.questions.attribute import Attribute
from screenpy.resolutions.is_equal_to import IsEqualTo

scenarios("../features/payment_purchase.feature")


@pytest.fixture(autouse=True)
def conf_test(request) -> None:
    global customer
    customer = AnActor.named("Customer")
    chrome_driver = DriverManager.get_chrome_driver()
    customer.who_can(BrowseTheWeb.using(chrome_driver))

    def end():
        customer.should(End.test())

    request.addfinalizer(end)


@given("a customer sing in into the ecommerce site")
def a_customer_sing_in_into_the_ecommerce_site():
    customer.was_able_to(
        GotoEcommerce.home(),
        FillOut.sing_up_form(),
        RequestUser.creation(),
        Wait(FIVE).second_for(home_page.BTN_WELCOME_USER),
    )


@given("the customer adds products to shopping car")
def the_customer_adds_products_to_shopping_car():
    customer.attempts_to(
        AddPopularProducts.to_shopping_car(),
        GoToCheckOut.screen()
    )


@when("the customer completes the payment for the purchase")
def the_customer_completes_the_payment_for_the_purchase():
    #ToDo -> Select payment method after bug solution
    customer.attempts_to(
        Take.screenshot()
    )


@then("the customer should be successfully payment process")
def the_customer_should_be_successfully_payment_process():
    # ToDo -> Add another validations for the test after bug solution
    # This test is failing for bug in payment process
    customer.should(
        See.the(
            Attribute("disabled").of_the(checkout_page.BTN_CONTINUE_TO_PAYMENT),
            IsEqualTo("false")
        )
    )
