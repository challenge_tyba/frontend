import pytest
from pytest_bdd import scenarios, given, when, then
from screenpy import AnActor
from screenpy.abilities import BrowseTheWeb
from screenpy.actions import See, Clear, Click
from screenpy.actions import Wait, Pause
from screenpy.directions import noted_under
from screenpy.questions.attribute import Attribute
from screenpy.questions.text import Text
from screenpy.resolutions.is_equal_to import IsEqualTo
from screenpy.resolutions.reads_exactly import ReadsExactly

import user_interface.home_page as home_page
import user_interface.sing_up_page as sing_up_page
from driver.driver_manager import DriverManager
from tasks.commons.end_test import End
from tasks.ecommerce.fill_up_sign_up_form import FillOut
from tasks.ecommerce.go_to_ecommerce_site import GotoEcommerce
from tasks.ecommerce.request_new_user_creation import RequestUser
from utils.constants.waits_constants import FIVE, TWO

scenarios("../features/user_sign_up.feature")


@pytest.fixture(autouse=True)
def conf_test(request) -> None:
    global customer
    customer = AnActor.named("Customer")
    chrome_driver = DriverManager.get_chrome_driver()
    customer.who_can(BrowseTheWeb.using(chrome_driver))

    def end():
        customer.should(End.test())

    request.addfinalizer(end)


@given("a customer in the ecommerce home site")
def a_customer_in_the_ecommerce_home_site():
    customer.was_able_to(
        GotoEcommerce.home()
    )


@when("the customer complete all information to create the user")
def the_customer_complete_all_information_to_create_the_user():
    customer.attempts_to(
        FillOut.sing_up_form(),
        RequestUser.creation()
    )


@when("the customer complete all information to create the user without accepted term and conditions")
def the_customer_complete_all_information_to_create_the_user_without_accepted_term_and_conditions():
    customer.attempts_to(
        FillOut.sing_up_form()
    )


@when("the customer has any input empty in the form to create the user")
def the_customer_has_any_input_empty_in_the_form_to_create_the_user():
    customer.attempts_to(
        FillOut.sing_up_form(),
        Clear.the_text_from(sing_up_page.INPUT_FIRST_NAME),
        Click.on_the(sing_up_page.CHECK_BOX_AGREE)
    )


@then("the customer should be create a new user successfully")
def the_customer_should_be_create_a_new_user_successfully():
    customer.should(
        Wait(FIVE).second_for(home_page.BTN_WELCOME_USER),
        See.the(
            Text.of(home_page.BTN_WELCOME_USER),
            ReadsExactly(noted_under("username"))
        )
    )


@then("the customer should see registration button disabled")
def the_customer_should_see_registration_button_disabled():
    customer.should(
        Pause.for_(TWO).second_because("controls for button"),
        See.the(
            Attribute("disabled").of_the(sing_up_page.BTN_REGISTER), IsEqualTo("true"))
    )
