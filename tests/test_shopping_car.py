import pytest
from pytest_bdd import scenarios, given, when, then
from screenpy import AnActor
from screenpy.abilities import BrowseTheWeb
from screenpy.actions import See, Click
from screenpy.actions import Wait
from screenpy.questions.element import Element
from screenpy.resolutions.is_visible import IsVisible

import user_interface.home_page as home_page
import user_interface.product_page as product_page
import user_interface.shopping_car_page as shopping_car_page
from driver.driver_manager import DriverManager
from tasks.commons.end_test import End
from tasks.ecommerce.add_products_to_car import AddPopularProducts
from tasks.ecommerce.fill_up_sign_up_form import FillOut
from tasks.ecommerce.go_to_checkout_screen import GoToCheckOut
from tasks.ecommerce.go_to_ecommerce_site import GotoEcommerce
from tasks.ecommerce.request_new_user_creation import RequestUser
from utils.constants.waits_constants import FIVE, TEN

scenarios("../features/shopping_car.feature")


@pytest.fixture(autouse=True)
def conf_test(request) -> None:
    global customer
    customer = AnActor.named("Customer")
    chrome_driver = DriverManager.get_chrome_driver()
    customer.who_can(BrowseTheWeb.using(chrome_driver))

    def end():
        customer.should(End.test())

    request.addfinalizer(end)


@given("a customer sing in into the ecommerce site")
def a_customer_sing_in_into_the_ecommerce_site():
    customer.was_able_to(
        GotoEcommerce.home(),
        FillOut.sing_up_form(),
        RequestUser.creation(),
        Wait(FIVE).second_for(home_page.BTN_WELCOME_USER),
    )


@when("the customer adds products to shopping car")
def the_customer_adds_products_to_shopping_car():
    customer.attempts_to(
        AddPopularProducts.to_shopping_car(),
        GoToCheckOut.screen()
    )


@when("the customer goes to checkout with the shopping car empty")
def the_customer_goes_to_checkout_with_the_shopping_car_empty():
    customer.attempts_to(
        Click.on_the(product_page.MENU_CAR)
    )


@then("the customer should see the checkout successfully")
def the_customer_should_see_the_checkout_successfully():
    customer.should(
        See.the(
            Element(shopping_car_page.SECTION_SHIPPING_DETAILS), IsVisible()
        ),
        See.the(
            Element(shopping_car_page.SECTION_PAYMENT_METHOD), IsVisible()
        )
    )


@then("the customer should see label empty car")
def the_customer_should_see_label_empty_car():
    customer.should(
        Wait(TEN).seconds_for_the(shopping_car_page.BTN_CONTINUE_SHOPPING),
        See.the(
            Element(shopping_car_page.BTN_CONTINUE_SHOPPING), IsVisible()
        ),
        See.the(
            Element(shopping_car_page.LABEL_CARD_EMPTY), IsVisible()
        )
    )
