from screenpy import Target
from selenium.webdriver.common.by import By

BTN_USER = Target.the("button user").located_by((By.ID, "menuUserSVGPath"))
BTN_CREATE_NEW_ACCOUNT = Target.the("button create new account").located_by((By.XPATH, "//*[@class='create-new-account ng-scope']"))
FORM_CREATE_NEW_ACCOUNT = Target.the("form create new account").located_by((By.ID, "formCover"))
INPUT_USERNAME = Target.the("input username").located_by((By.XPATH, "//*[@name='usernameRegisterPage']"))
INPUT_EMAIL = Target.the("input email").located_by((By.XPATH, "//*[@name='emailRegisterPage']"))
INPUT_PASSWORD = Target.the("input password").located_by((By.XPATH, "//*[@name='passwordRegisterPage']"))
INPUT_CONFIRM_PASSWORD = Target.the("input confirm password").located_by((By.XPATH, "//*[@name='confirm_passwordRegisterPage']"))
INPUT_FIRST_NAME = Target.the("input first name").located_by((By.XPATH, "//*[@name='first_nameRegisterPage']"))
INPUT_LAST_NAME = Target.the("input last name").located_by((By.XPATH, "//*[@name='last_nameRegisterPage']"))
INPUT_PHONE_NUMBER = Target.the("input phone number").located_by((By.XPATH, "//*[@name='phone_numberRegisterPage']"))
INPUT_CITY = Target.the("input city").located_by((By.XPATH, "//*[@name='cityRegisterPage']"))
INPUT_ADDRESS = Target.the("input address").located_by((By.XPATH, "//*[@name='addressRegisterPage']"))
INPUT_STATE = Target.the("input state").located_by((By.XPATH, "//*[@name='state_/_province_/_regionRegisterPage']"))
INPUT_POSTAL_CODE = Target.the("input postal code").located_by((By.XPATH, "//*[@name='postal_codeRegisterPage']"))
CHECK_BOX_AGREE = Target.the("check box agree terms and conditions").located_by((By.XPATH, "//*[@name='i_agree']"))
BTN_REGISTER = Target.the("button register").located_by((By.ID, "register_btnundefined"))


