from screenpy import Target
from selenium.webdriver.common.by import By

XPATH_DETAILS_POPULAR_ITEMS = "//*[@id='popular_items']//*[contains(@id, 'details_')]"
BTN_PLUS_PRODUCT = Target.the("button plus to add product").located_by((By.XPATH, "//*[@class='plus']"))
BTN_SAVE_TO_CAR = Target.the("button save to car").located_by((By.XPATH, "//*[@name='save_to_cart']"))
BTN_CHECK_OUT = Target.the("button check out").located_by((By.ID, "checkOutPopUp"))
MENU_CAR = Target.the("menu shopping car").located_by((By.ID, "menuCart"))
USER_CAR = Target.the("card for user shopping car").located_by((By.ID, "userCart"))
BTN_GO_TO_HOME = Target.the("button go to home").located_by((By.XPATH, "//*[@class='pages fixedImportant ng-scope']//*[@translate='HOME']"))
