from screenpy import Target
from selenium.webdriver.common.by import By

SECTION_SHIPPING_DETAILS = Target.the("section shipping details").located_by((By.XPATH, "//*[contains(@class, 'roboto-regular ng-binding') and contains(text(),'1. SHIPPING DETAILS')]"))
SECTION_PAYMENT_METHOD = Target.the("section shipping details").located_by((By.XPATH, "//*[contains(@class, 'roboto-regular ng-binding') and contains(text(),'2. PAYMENT METHOD')]"))
BTN_CONTINUE_SHOPPING = Target.the("button continue shopping").located_by((By.XPATH, "//*[@class='a-button ng-scope' and contains(text(),'CONTINUE SHOPPING')]"))
LABEL_CARD_EMPTY = Target.the("label car empty").located_by((By.XPATH, "//*[@class='roboto-bold ng-scope' and contains(text(),'Your shopping cart is empty')]"))

